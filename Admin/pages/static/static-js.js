$(document).ready(function () {
  //PIE CHART CHỨA SỐ LƯỢNG LOẠI PIZZA ĐƯỢC YÊU THÍCH
  var donutData = {
    labels: ["Hawaii", "Seafood", "Bacon", "Khác"],
    datasets: [
      {
        data: [30, 25, 20, 10],
        backgroundColor: ["#f56954", "#00a65a", "#f39c12", "#00c0ef"],
      },
    ], //đổi dữ liệu data của dataset
    changeData: function () {
      this.datasets.map((paramData) => {
        return (paramData.data = [
          gNumberOfHawaii,
          gNumberOfSeafood,
          gNumberOfBacon,
          gNumberOfAnother,
        ]);
      });
    },
    
  };
  //-------------
  //- PIE CHART -
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
  var pieData = donutData;
  var pieOptions = {
    maintainAspectRatio: false,
    responsive: true,
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  var myPieChart = new Chart(pieChartCanvas, {
    type: "pie",
    data: pieData,
    options: pieOptions,
  });
  

  
  //-------------
  //- DONUT CHART - BIỂU ĐỒ DONUT HIỂN THỊ TRẠNG THÁI CỦA ĐƠN HÀNG
  //-------------
  // Get context with jQuery - using jQuery's .get() method.
  var donutChartCanvas = $("#donutChart").get(0).getContext("2d");
  var StatusData = {
    labels: ["Open", "Confirmed", "Cancel"],
    datasets: [
      {
        data: [700, 300, 100],
        backgroundColor: [
          "rgba(241, 129, 207, 0.603)",
          "rgb(241, 129, 129)",
          "rgba(238, 84, 174, 0.767)",
        ],
      },
    ],
    changeDonutData: function () {
      this.datasets.map((paramData) => {
        return (paramData.data = [
          gOpenNumber,
          gConfirmedNumber,
          gCancelNumber,
        ]);
      });
    },
  };
  var donutOptions = {
    maintainAspectRatio: false,
    responsive: true,
  };
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  var myDonutChart = new Chart(donutChartCanvas, {
    type: "doughnut",
    data: StatusData,
    options: donutOptions,
  });

  /*
   * END DONUT CHART
   */
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //tạo biến chứa tổng các đơn hàng gọi từ API
  var gOrderDB = {
    orders: [], // vùng chứa các đơn hàng
    changeNullValue: function () {
      //phương thức chuyển các giá trị null trong obj thành rỗng trước khi lọc
      this.orders.forEach(function (paramOrder) {
        for (var key in paramOrder) {
          if (paramOrder[key] == null) {
            paramOrder[key] = "";
          }
        }
      });
    },
    filterVoucher: function () {
      // phương thức lọc voucher (TRẢ VỀ 1 MẢNG CHỨA CÁC ORDER CÓ SỬ DỤNG VOUCHER)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.idVourcher != "";
      });
      return vFilterResult;
    },
    filterDrink: function () {
      // phương thức lọc đồ uống (TRẢ VỀ 1 MẢNG CHỨA CÁC ORDER KHÔNG CÓ ĐẶT ĐỒ UỐNG)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.idLoaiNuocUong == "";
      });
      return vFilterResult;
    },
    filterSeafoodPizzaType: function () {
        //phương thức lọc loại pizza hải sản (TRẢ VỀ 1 MẢNG CHỨA PIZZA HẢI SẢN)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.loaiPizza.toLowerCase() == "seafood";
      });
      return vFilterResult;
    },
    filterHawaiiPizzaType: function () {
        //phương thức lọc loại pizza HAWAII (TRẢ VỀ 1 MẢNG CHỨA PIZZA HAWAII)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.loaiPizza.toLowerCase() == "hawaii";
      });
      return vFilterResult;
    },
    filterBaconPizzaType: function () {
        //phương thức lọc loại pizza BACON (TRẢ VỀ 1 MẢNG CHỨA PIZZA BACON)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.loaiPizza.toLowerCase() == "bacon";
      });
      return vFilterResult;
    }, 
    filterAnotherPizzaType: function () {
        ////phương thức lọc loại pizza KHÁC (TRẢ VỀ 1 MẢNG CHỨA PIZZA KHÁC)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return (
          paramOrder.loaiPizza.toLowerCase() != "bacon" &&
          paramOrder.loaiPizza.toLowerCase() != "hawaii" &&
          paramOrder.loaiPizza.toLowerCase() != "seafood"
        );
      });
      return vFilterResult;
    },
    filterOpenStatus: function () {
        //phương thức lọc trạng thái OPEN của ORDER (TRẢ VỀ 1 MẢNG CHỨA TRẠNG THÁI OPEN)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.trangThai.toLowerCase() == "open";
      });
      return vFilterResult;
    },
    filterConfirmStatus: function () {
        //phương thức lọc trạng thái CONFIRM của ORDER (TRẢ VỀ 1 MẢNG CHỨA TRẠNG THÁI CONFIRM)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.trangThai.toLowerCase() == "confirmed";
      });
      return vFilterResult;
    },
    filterCancelStatus: function () {
        //phương thức lọc trạng thái CANCEL của ORDER (TRẢ VỀ 1 MẢNG CHỨA TRẠNG THÁI CANCEL)
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return paramOrder.trangThai.toLowerCase() == "cancel";
      });
      return vFilterResult;
    }
  };
  //các biến chứa loại voucher và số lượng
  var gVoucherFilterResult = [];
  var gVoucherNumber = 0;
  //các biến chứa loại nuowsc uong và số lượng
  var gDrinkFilterResult = [];
  var gDrinkNumber = 0;
  //các biến chứa loại pizza và tổng số loại pizza
  var gSeafoodPizzaFilterResult = [];
  var gHawaiPizzaFilterResult = [];
  var gBaconPizzaFilterResult = [];
  var gAnotherPizzaFilterResult = [];
  var gNumberOfHawaii = 0;
  var gNumberOfSeafood = 0;
  var gNumberOfBacon = 0;
  var gNumberOfAnother = 0;

  //các biến chứa loại status và tổng số status
  var gOpenStatus = [];
  var gConfirmedStatus = [];
  var gCancelStatus = [];

  var gOpenNumber = 0;
  var gConfirmedNumber = 0;
  var gCancelNumber = 0;

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  //thực thi sự kiện load trang
  onPageLoading();
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //hàm xử lý sự kiện load trang
  function onPageLoading() {
    "use strict";
    //Load dữ liệu đơn hàng vào bảng
    callApiToGetAllOrder();
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //hàm gọi api lấy tổng số đơn hàng
  function callApiToGetAllOrder() {
    "use strict";
    $.ajax({
      type: "GET",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      dataType: "json",
      success: function (paramResObj) {
        gOrderDB.orders = paramResObj;
        //thống kê tổng số đơn hàng
        $("#sale").html(paramResObj.length);

        //chuyển đổi dữ liệu null thành rỗng
        gOrderDB.changeNullValue();

        //lọc voucher (gọi phương thức lọc voucher)
        gVoucherFilterResult = gOrderDB.filterVoucher();
        gVoucherNumber = gVoucherFilterResult.length; // thống kê số lượng mã voucher đã sử dụng
        $("#voucher").html(gVoucherNumber); // in số liệu thống kê ra html

        //lọc người dùng không dùng đồ uông ( gọi phương thức lọc đồ uống)
        gDrinkFilterResult = gOrderDB.filterDrink();
        gDrinkNumber = gDrinkFilterResult.length; // thống kê số lượng người dùng không đặt đồ uống
        $("#drink").html(gDrinkNumber); // in số liệu thống kê ra html

        //hiển thị thống kê số liệu loại pizza yêu thích
        changePieChartValue();

        //hiển thị thống kê trạng thái đơn hàng
        changeStatusChart();
      },
      error: function (paramErr) {
        alert(paramErr);
      },
    });
  }
  // hàm xử lý hiển thị số liệu loại pizza yêu thích ( biểu đồ PIE CHART)
  function changePieChartValue() {
    "use strict";
    //lọc dữ liệu các loại pizza 
    gHawaiPizzaFilterResult = gOrderDB.filterHawaiiPizzaType();
    gSeafoodPizzaFilterResult = gOrderDB.filterSeafoodPizzaType();
    gBaconPizzaFilterResult = gOrderDB.filterBaconPizzaType();
    gAnotherPizzaFilterResult = gOrderDB.filterAnotherPizzaType();
    //lấy số lượng các loại pizza
    gNumberOfHawaii = gHawaiPizzaFilterResult.length;
    gNumberOfSeafood = gSeafoodPizzaFilterResult.length;
    gNumberOfBacon = gBaconPizzaFilterResult.length;
    gNumberOfAnother = gAnotherPizzaFilterResult.length;
    //hiển thị số lượng các loại pizza lên biểu đồ
    donutData.changeData();
     //update lại dữ liệu của pie chart dựa vào số liệu các loại pizza ưa thích
    myPieChart.update()
  }
  //hàm xử lý hiển thị số liệu trạng thái đơn hàng
  function changeStatusChart() {
    "use strict";
    //lọc dữ liệu trạng thái của đơn hàng
    gOpenStatus = gOrderDB.filterOpenStatus();
    gConfirmedStatus = gOrderDB.filterConfirmStatus();
    gCancelStatus = gOrderDB.filterCancelStatus();
    //lấy số lượng trạng thái của đơn hàng
    gOpenNumber = gOpenStatus.length;
    gConfirmedNumber = gConfirmedStatus.length;
    gCancelNumber = gCancelStatus.length;
    //hiển thị số lượng trạng thái đơn hàng lên biểu đồ
    StatusData.changeDonutData();
    //update lại dữ liệu của donut chart dựa vào số liệu trạng thái đơn hàng
    myDonutChart.update()
  }
});
