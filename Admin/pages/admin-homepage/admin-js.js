$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

  /** Các biến toàn cục hằng số Form mode: 4 trạng thái của form. Mặc định sẽ là Normal
   *
   ** Khi ấn vào nút Thêm, cần đổi biến trạng thái về trạng thái Insert
   ** Khi ấn vào nút Sửa, cần đổi biến trạng thái về trạng thái Update
   ** Khi ấn vào nút Xóa, cần đổi biến trạng thái về trạng thái Delete
   *
   * Tại một thời điểm, trạng thái của form luôn là 1 trong 4 trạng thái
   */
  var gFORM_MODE_NORMAL = "Normal";
  var gFORM_MODE_INSERT = "Insert";
  var gFORM_MODE_UPDATE = "Update";
  var gFORM_MODE_DELETE = "Delete";

  // biến toàn cục cho trạng thái của form: mặc định ban đầu là trạng thái Normal
  var gFormMode = gFORM_MODE_NORMAL;
  //Biến chứa dữ liệu đơn hàng lấy từ api
  var gOrderDB = {
    orders: [], // vùng chứa các đơn hàng lấy từ api
    changeNullValue: function () {
      //phương thức chuyển các giá trị null trong obj thành rỗng trước khi lọc
      this.orders.forEach(function (paramOrder) {
        for (var key in paramOrder) {
          if (paramOrder[key] == null) {
            paramOrder[key] = "";
          }
        }
      });
    },
    //phương thức lọc dữ liệu
    /** INP:
     *
     * @param {*} paramFilterObj : đối tượng muốn lọc (mã order, số điện thoại, trạng thái và loại pizza)
     * OUT:
     * @returns vFilterResult: kết quả lọc
     * nếu có nhập sẽ trả về kết quả, nếu không nhập sẽ trả về toàn bộ danh sách orders
     */
    filterOrder: function (paramFilterObj) {
      var vFilterResult = [];
      vFilterResult = this.orders.filter((paramOrder) => {
        return (
          (paramOrder.orderId
            .toLowerCase()
            .includes(paramFilterObj.orderId.toLowerCase()) ||
            paramFilterObj.orderId === "") &&
          (paramOrder.soDienThoai.includes(paramFilterObj.soDienThoai) ||
            paramFilterObj.soDienThoai === "") &&
          (paramOrder.trangThai == paramFilterObj.trangThai ||
            paramFilterObj.trangThai === "") &&
          (paramOrder.loaiPizza.toUpperCase() ==
            paramFilterObj.loaiPizza.toUpperCase() ||
            paramFilterObj.loaiPizza === "")
        );
      });
      return vFilterResult;
    }, //phương thức thay thế khi sửa dữ liệu thành công trên api
    replaceObjAfterUpdated: function (paramResponseObj) {
      this.orders.map((paramOrder, paramIndex) => {
        if (paramOrder.id === paramResponseObj.id) {
          return this.orders.splice(paramIndex, 1, paramResponseObj);
        }
      });
    }, //phương thức add order mới vào
    addNewOrder: function (paramOrder) {
      this.orders.push(paramOrder);
    },
  };

  //tạo biến chứa id của đơn hàng
  var gId = [];
  //tạo biến chứa order id của đơn hàng
  var gOrderId = [];
  //Tạo biến chứa thuộc tính của bảng
  const gORDER_COLUMNS = [
    "orderId",
    "hoTen",
    "soDienThoai",
    "kichCo",
    "loaiPizza",
    "idLoaiNuocUong",
    "trangThai",
    "action",
  ];
  //thứ tự các cột
  const gCOL_ORDER_ID = 0;
  const gCOL_HO_TEN = 1;
  const gCOL_DIEN_THOAI = 2;
  const gCOL_KICH_CO = 3;
  const gCOL_LOAI_PIZZA = 4;
  const gCOL_LOAI_NUOC = 5;
  const gCOL_TRANG_THAI = 6;
  const gCOL_ACTION = 7;

  //TẠO Bảng
  var gOrderTable = $("#order-table").DataTable({
    ordering: false,
    columns: [
      { data: gORDER_COLUMNS[gCOL_ORDER_ID] },
      { data: gORDER_COLUMNS[gCOL_HO_TEN] },
      { data: gORDER_COLUMNS[gCOL_DIEN_THOAI] },
      { data: gORDER_COLUMNS[gCOL_KICH_CO] },
      { data: gORDER_COLUMNS[gCOL_LOAI_PIZZA] },
      { data: gORDER_COLUMNS[gCOL_LOAI_NUOC] },
      { data: gORDER_COLUMNS[gCOL_TRANG_THAI] },
      { data: gORDER_COLUMNS[gCOL_ACTION] },
    ],
    columnDefs: [
      {
        //ĐỊNH NGHĨA LẠI CỘT ACTION (tạo 2 nút edit và delete)
        targets: gCOL_ACTION,
        className: "align-middle",
        defaultContent: `
                    <button class="btn order-detail text-white  btn-sm" style="background-color: #6FB98F"><i class="fas fa-edit" style="font-size:10px;"></i></button>
                    <button class="btn delete-detail text-white btn-sm" style="background-color: #8EBA43"><i class="fas fa-trash" style="font-size:10px;"></i></button>
                `,
      },
      {
        // định dạng lại css cho các cột
        targets: [
          gCOL_ORDER_ID,
          gCOL_HO_TEN,
          gCOL_DIEN_THOAI,
          gCOL_KICH_CO,
          gCOL_LOAI_PIZZA,
          gCOL_LOAI_NUOC,
          gCOL_TRANG_THAI,
        ],
        className: "align-middle",
      },
    ],
  });
  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
  //thực thi hàm load lại trang
  onPageLoading();

  //PHẦN LỌC DỮ LIỆU
  //Gán sự kiện cho nút lọc dữ liệu đơn hàng
  $("#btn-filter-data").on("click", onBtnFilterOrderClick);

  //PHẦN NÚT THÊM MỚI ĐƠN HÀNG
  //gán sự kiện cho nút tạo thêm đơn hàng mới
  $("#btn-add-order").on("click", onBtnCreateNewOrderClick);
  //gán sự kiện cho nút xác nhận đơn hàng trong modal
  $("#btn-confirm-order").on("click", onBtnConfirmOrderClick);
  //gán sư kiện cho nút huỷ đơn hàng
  $("#delete-modal").on("hidden.bs.modal", function () {
    //reset lại các trường dữ liệu trong modal
    resetForm();
  });

  //PHẦN NÚT EDIT
  //gán sự kiện cho nút chi tiết trong bảng (nút edit)
  $("#order-table").on("click", ".order-detail", function () {
    onBtnOrderDetailClick(this);
  });

  //reset dữ liệu khi click nút tắt modal chi tiết đơn hàng
  $("#order-detail-modal").on("hidden.bs.modal", function () {
    //reset lại các trường dữ liệu trong modal
    resetForm();
  });

  //PHẦN NÚT DELETE
  //gán sự kiện cho nút xoá đơn hàng trong bảng (nút delete)
  $("#order-table").on("click", ".delete-detail", function () {
    onBtnDeleteOrderClick(this);
  });
  //gán sự kiện cho nút xác nhận xoá đơn hàng
  $("#btn-confirm-delete").on("click", onBtnConfirmDeleteClick);

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //hàm xử lý sự kiện load trang
  function onPageLoading() {
    "use strict";
    //Set trạng thái normal
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);
    //Load dữ liệu tất cả các đơn hàng gọi từ api vào bảng
    callApiToLoadOrderToTable();
    //load dữ liệu đồ uống vào select chọn đồ uống trong modal chi tiết đơn hàng
    callApiGetDrinkListLoadToSelect();
  }

  //Hàm xử lý sự kiện ấn nút lọc dữ liệu đơn hàng
  function onBtnFilterOrderClick() {
    "use strict";
    //tạo đối tượng cần lọc
    var vFilterObj = {
      orderId: "",
      soDienThoai: "",
      trangThai: "",
      loaiPizza: "",
    };
    //tạo biến chứa kết quả lọc
    var vFilterResult = [];
    //Thu thập dữ liệu cho đối tượng cần lọc cần lọc
    vFilterObj.orderId = $("#filter-inp-orderid").val();
    vFilterObj.soDienThoai = $("#filter-inp-sodienthoai").val();
    vFilterObj.trangThai = $("#filter-select-trangthai").val();
    vFilterObj.loaiPizza = $("#filter-select-loaipizza").val();
    //Lọc dữ liệu (gọi phương thức lọc dữ liệu trong gOrderDB)
    vFilterResult = gOrderDB.filterOrder(vFilterObj);

    //load dữ liệu đã lọc được vào bảng
    loadDataToTable(vFilterResult);
  }

  //Hàm xử lý sự kiện ấn nút THÊM MỚI ĐƠN HÀNG
  function onBtnCreateNewOrderClick() {
    "use strict";
    //Set trạng thái insert
    gFormMode = gFORM_MODE_INSERT;
    $("#div-form-mod").html(gFormMode);
    //Hiển thị modal chi tiết đơn hàng
    $("#order-detail-modal").modal("show");
    //chọn combo nào sẽ hiển thị thông tin combo đó lên các trường dữ liêu trong modal chi tiết đơn hàng
    $("#modal-select-kichco").on("change", function () {
      onSelectChangeValue(this);
    });
  }

  //Hàm xử lý sự kiện ấn nút xác nhận đơn hàng trong modal chi tiết đơn hàng
  function onBtnConfirmOrderClick() {
    "use strict";
    //nếu ở trạng thái thêm mới đơn hàng INSERT
    if (gFormMode === gFORM_MODE_INSERT) {
      //tạo đối tượng order muốn thêm mới
      var vOrderRequest = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
      };
      //thu thập thông tin cho order
      getOrderInfo(vOrderRequest);
      //kiểm tra các thông tin có hợp lệ hay không
      var vIsValidated = validateOrderInfo(vOrderRequest);
      //nếu các thông tin hợp lệ thì thực hiện các xử lý như sau:
      if (vIsValidated) {
        if (vOrderRequest.idVourcher === "") {
          // trường hợp không nhập mã voucher
          //gọi api tạo order mới
          callApiCreateNewOrder(vOrderRequest);
        } else {
          // trường hợp có nhập mã voucher
          //gọi api kiểm tra xem mã voucher đã nhập có hợp lệ hay không
          callApiToCheckVoucherCode(vOrderRequest.idVourcher, vOrderRequest);
        }
      }
    }
    //Nếu ở trạng thái edit đơn hàng (UPDATE)
    else {
      //tạo đối tượng chứa trạng thái muốn cập nhật
      var vObjectRequest = {
        trangThai: "",
      };
      //Thu thập dữ liệu cho trạng thái
      vObjectRequest.trangThai = $("#select-status").val();
      //Validate dữ liệu trạng thái
      if (vObjectRequest.trangThai === "") {
        // trường hợp không chọn trạng thái
        //hiển thị modal cảnh báo
        var vModalWarning = $("#modal-warning");
        var vModalWarningBody = $("#modal-body");

        vModalWarning.modal("show");
        vModalWarningBody.html("Xin vui lòng chọn trạng thái");
      } else {
        // trường hợp chọn trạng thái
        //Gọi Api update đơn hàng
        callApiToUpdateOrderStatus(vObjectRequest);
        //báo cập nhật thành công
        alert("dữ liệu cập nhật thành công");
        //tắt modal sau khi update thành công
        $("#order-detail-modal").modal("hide");
        //reset các trường dữ liệu form trong modal chi tiết đơn hàng
        resetForm();
        //reset trở về trạng thái normal
        gFormMode = gFORM_MODE_NORMAL;
        $("#div-form-mod").html(gFormMode);
      }
    }
  }

  //Hàm xử lý sự kiện ấn nút chi tiết
  function onBtnOrderDetailClick(paramDetailButton) {
    "use strict";

    //Set trạng thái update khi ấn nút edit
    gFormMode = gFORM_MODE_UPDATE;
    $("#div-form-mod").html(gFormMode);

    //lấy dữ liệu của dòng
    var vRowSelected = $(paramDetailButton).parents("tr");
    var vDataTableRow = gOrderTable.row(vRowSelected);
    var vRowData = vDataTableRow.data();

    //Lấy id của dòng
    gId = vRowData.id;
    //lấy order id của dòng
    gOrderId = vRowData.orderId;
    //Hiện modal chi tiết đơn hàng
    $("#order-detail-modal").modal("show");

    //Thêm trường dữ liệu html vào modal chi tiết đơn hàng
    addMoreHtmlElement();
    //Thêm read only vào các trường dữ liệu không được sửa trên modal
    addReadOnlyToForm();
    //Hiển thị dữ liệu đơn hàng lên modal
    showOrderDetailToModal(vRowData);
  }

  //Hàm xử lý sự kiện ấn nút xoá đơn hàng
  function onBtnDeleteOrderClick(paramDeleteButton) {
    "use strict";
    //Set trạng thái Delete
    gFormMode = gFORM_MODE_DELETE;
    $("#div-form-mod").html(gFormMode);

    //lấy dữ liệu của dòng
    var vRowSelected = $(paramDeleteButton).parents("tr");
    var vDataTableRow = gOrderTable.row(vRowSelected);
    var vRowData = vDataTableRow.data();

    //Lấy id của dòng
    gId = vRowData.id;
    //lấy order id của dòng
    gOrderId = vRowData.orderId;

    //Hiện modal delete
    $("#delete-modal").modal("show");
  }

  //Hàm xử lý sự kiện ấn nút xác nhận xoá đơn hàng
  function onBtnConfirmDeleteClick() {
    "use strict";
    //gọi api xoá đơn hàng
    callApiToDeleteOrder();
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Hàm xử lý sự kiện gọi Api tất cả các đơn hàng và load dữ liệu đơn hàng vào bảng
  function callApiToLoadOrderToTable() {
    "use strict";
    $.ajax({
      type: "GET",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      dataType: "json",
      success: function (paramResObj) {
        gOrderDB.orders = paramResObj;
        //chuyển đổi các thuộc tính null thành rỗng
        gOrderDB.changeNullValue();
        //load tất cả dữ liệu đơn hàng trả về lên bảng
        loadDataToTable(gOrderDB.orders);
      },
      error: function (paramErr) {
        alert(paramErr);
      },
    });
  }

  //Hàm xử lý load dữ liệu vào bảng
  /**
   * INP:
   * @param {*} paramOrderArr :danh sách các order
   * OUT:
   * load tất cả các order lên bảng
   */
  function loadDataToTable(paramOrderArr) {
    "use strict";
    gOrderTable.clear();
    gOrderTable.rows.add(paramOrderArr);
    gOrderTable.draw();
  }

  //Hàm gọi api lấy dữ liệu đồ uống load vào select modal chi tiết đơn hàng
  function callApiGetDrinkListLoadToSelect() {
    "use strict";
    $.ajax({
      url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
      type: "GET",
      dataType: "json",
      success: function (paramResObj) {
        //load danh sách đồ uống lấy từ api vào select trong modal chi tiết đơn hàng
        loadDrinkListToSelect(paramResObj);
      },
    });
  }

  //hàm load dữ liệu đồ uống vào select modal chi tiết đơn hàng
  /**
   * INP:
   * @param {*} paramDrinkArr :danh sách đồ uống lấy từ api về
   * OUT: load dữ liệu lên trên select loại đồ uống trên modal chi tiết đơn hàng
   */
  function loadDrinkListToSelect(paramDrinkArr) {
    "use strict";
    var vSelectDrink = $("#modal-select-loainuocuong");
    for (var bI = 0; bI < paramDrinkArr.length; bI++) {
      vSelectDrink.append(
        $("<option>", {
          value: paramDrinkArr[bI].maNuocUong,
          text: paramDrinkArr[bI].tenNuocUong,
        })
      );
    }
  }

  //Hàm xử lý lấy dữ liệu đường kính, salad... khi select kích cỡ combo thay đổi và hiển thị lên modal chi tiết đơn hàng
  function onSelectChangeValue() {
    "use strict";
    var vCombo = $("#modal-select-kichco").val(); // tạo biến chứa value kích cỡ combo đã chọn
    // nếu chọn combo cỡ S
    if (vCombo == "S") {
      $("#modal-inp-duongkinh").val("20");
      $("#modal-inp-suonnuong").val("2");
      $("#modal-inp-salad").val("200");
      $("#modal-inp-soluongnuoc").val("2");
      $("#modal-inp-thanhtien").val("150000");
      $("#modal-inp-thanhtoan").val(numberWithCommas(150000));
    }
    //nếu chọn combo cỡ M
    if (vCombo == "M") {
      $("#modal-inp-duongkinh").val("25");
      $("#modal-inp-suonnuong").val("4");
      $("#modal-inp-salad").val("300");
      $("#modal-inp-soluongnuoc").val("3");
      $("#modal-inp-thanhtien").val("200000");
      $("#modal-inp-thanhtoan").val(numberWithCommas(200000));
    }
    //nếu chọn combo cỡ L
    if (vCombo == "L") {
      $("#modal-inp-duongkinh").val("30");
      $("#modal-inp-suonnuong").val("8");
      $("#modal-inp-salad").val("500");
      $("#modal-inp-soluongnuoc").val("4");
      $("#modal-inp-thanhtien").val("250000");
      $("#modal-inp-thanhtoan").val(numberWithCommas(250000));
    }
  }

  //Hàm xử lý thu thập dữ liệu cho đơn hàng mới muốn tạo
  /**
   * INP:
   * @param {*} paramOrder :đối tượng order mới muốn tạo
   * OUT: thu thập thông tin thông qua các trường dữ liệu trên web (modal chi tiết đơn hàng) cho đối tượng
   *
   */
  function getOrderInfo(paramOrder) {
    "use strict";
    //thông tin kích cỡ
    paramOrder.kichCo = $("#modal-select-kichco").val();
    //thông tin đường kính
    paramOrder.duongKinh = $("#modal-inp-duongkinh").val();
    //thông tin sườn nướng
    paramOrder.suon = $("#modal-inp-suonnuong").val();
    //thông tin salad
    paramOrder.salad = $("#modal-inp-salad").val();
    //thông tin số lượng nước
    paramOrder.soLuongNuoc = $("#modal-inp-soluongnuoc").val();
    //thông tin thành tiền
    paramOrder.thanhTien = $("#modal-inp-thanhtien").val();
    //thông tin loại pizza
    paramOrder.loaiPizza = $("#modal-select-loaipizza").val();
    //thông tin mã voucher
    paramOrder.idVourcher = $("#modal-inp-mavoucher").val();
    //thông tin loại nước uống
    paramOrder.idLoaiNuocUong = $("#modal-select-loainuocuong").val();
    //thông tin họ và tên
    paramOrder.hoTen = $("#modal-inp-hovaten").val().trim();
    //thông tin email
    paramOrder.email = $("#modal-inp-email").val().trim();
    //thông tin số điện thoại
    paramOrder.soDienThoai = $("#modal-inp-sodienthoai").val().trim();
    //thông tin địa chỉ
    paramOrder.diaChi = $("#modal-inp-diachi").val().trim();
    //thông tin lời nhắn
    paramOrder.loiNhan = $("#modal-inp-loinhan").val().trim();
  }
  //Hàm thêm dấu phẩy vào số tiền hàng ngàn
  /**
   * INP:
   * @param {*} paramNumber: số tiền cần chuyển đổi
   * OUT:
   * @returns số tiền được thêm dấu phẩy vào hàng ngàn
   */
  function numberWithCommas(paramNumber) {
    "use strict";
    return paramNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  //Hàm validate dữ liệu đơn hàng muốn tạo
  /**
   * INP:
   * @param {*} paramOrderInfo : thông tin đối tượng order muốn tạo
   * OUT:
   * @returns false: thông tin không hợp lệ
   * true: thông tin hợp lệ
   */
  function validateOrderInfo(paramOrderInfo) {
    "use strict";
    //truy xuất đến modal cảnh báo
    var vModalWarning = $("#modal-warning");
    var vModalWarningBody = $("#modal-body");
    //validate họ tên
    if (paramOrderInfo.hoTen === "") {
      vModalWarning.modal("show");
      vModalWarningBody.html("Xin vui lòng nhập họ và tên");
      return false;
    }
    //validate email
    var vValidEmail = validateEmail(paramOrderInfo.email);
    if (!vValidEmail) {
      vModalWarning.modal("show");
      vModalWarningBody.html(
        "Email không hợp lệ. Vui lòng nhập đúng form email (xyz@example.com)"
      );
      return false;
    }
    //validate số điện thoại
    var vValidPhoneNumber = validatePhoneNumber(paramOrderInfo.soDienThoai);
    if (!vValidPhoneNumber) {
      vModalWarning.modal("show");
      vModalWarningBody.html("Xin vui lòng nhập số điện thoại");
      return false;
    }

    //validate địa chỉ
    if (paramOrderInfo.diaChi === "") {
      vModalWarning.modal("show");
      vModalWarningBody.html("Xin vui lòng nhập địa chỉ");
      return false;
    }
    //validate kích cỡ combo
    if (paramOrderInfo.kichCo == null) {
      vModalWarning.modal("show");
      vModalWarningBody.html("Xin vui lòng chọn kích cỡ");
      return false;
    }
    //validate loại pizza
    if (paramOrderInfo.loaiPizza == null) {
      vModalWarning.modal("show");
      vModalWarningBody.html("Xin vui lòng chọn loại pizza");
      return false;
    }
    //validate loại nước uống
    if (paramOrderInfo.idLoaiNuocUong == null) {
      vModalWarning.modal("show");
      vModalWarningBody.html("Xin vui lòng chọn loại nước uống");
      return false;
    }

    //validate mã voucher
    if (isNaN(paramOrderInfo.idVourcher)) {
      vModalWarning.modal("show");
      vModalWarningBody.html("Mã voucher không hợp lệ");
      //mã voucher không hợp lệ sẽ trả về voucher rỗng
      paramOrderInfo.idVourcher = "";
      //hiển thị số tiền cần thanh toán
      var vConvertMoney = numberWithCommas(paramOrderInfo.thanhTien);
      $("#modal-inp-thanhtoan").val(vConvertMoney);
      return false;
    }
    return true;
  }
  //hàm validate số điện thoại
  /**
   * INP:
   * @param {*} paramPhoneNum : số điện thoại nhập vào từ modal chi tiết đơn hàng
   * OUT:
   * @returns  false: số điện thoại không hợp lệ
   * true: số điện thoại hợp lệ
   */
  function validatePhoneNumber(paramPhoneNum) {
    "use strict";
    //biến chứa dạng số điện thoại
    var vFormat = /^0([1-2]\d{9}|[1-9]\d{8})$/;
    if (!vFormat.test(paramPhoneNum)) {
      return false;
    }
    return true;
  }

  //Hàm validate email
  /**
   * INP:
   * @param {*} paramEmail : email nhập vào trong modal chi tiết đơn hàng
   * OUT:
   * @returns true: Email hợp lệ
   * false: Email không hợp lệ
   */
  function validateEmail(paramEmail) {
    "use strict";
    //biến chứa dạng email
    var vFormat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail === "") {
      // trong trường hợp không nhập email
      return true;
    }
    if (paramEmail != "") {
      // trong trường hợp có nhập email
      if (!vFormat.test(paramEmail)) {
        return false;
      }
      return true;
    }
  }

  //Hàm gọi api để tạo order mới
  /**
   * INP:
   * @param {*} paramOrder : đối tượng chứa order mới muốn tạo
   * OUT:
   * tạo order thành công lên api, load order mới lên bảng
   */

  function callApiCreateNewOrder(paramOrder) {
    "use strict";
    $.ajax({
      type: "POST",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramOrder),
      success: function (paramResObj) {
        //tạo thành công
        //báo cập nhật thành công
        alert("dữ liệu cập nhật thành công");
        //Tắt modal chi tiết đơn hàng
        $("#order-detail-modal").modal("hide");
        //reset trở về trạng thái normal
        gFormMode = gFORM_MODE_NORMAL;
        $("#div-form-mod").html(gFormMode);
        //reset lại các trường input trong modal
        resetForm();
        //hiển thị modal thông tin mã đơn hàng sau khi tạo thành công
        $("#success-order-modal").modal("show");
        //hiển thị order id
        $("#success-modal-inp-madonhang").val(paramResObj.orderId);
        //Thêm order mới vào danh sách
        gOrderDB.addNewOrder(paramResObj);
        //load dữ liệu đã update lên bảng
        loadDataToTable(gOrderDB.orders);
        //reload lại trang web
        $("#success-order-modal").on("hidden.bs.modal", function () {
          location.reload();
        });
      },
      error: function (paramErr) {
        // tạo thất bại
        alert(paramErr);
      },
    });
  }

  //Hàm gọi Api để check mã voucher xem có hợp lệ hay không
  /**
   * INP:
   * @param {*} paramvoucher : Mã voucher nhập vào từ modal chi tiết đơn hàng
   * @param {*} paramObj :đối tượng order tạo mới
   * OUT:
   * nếu voucher hợp lê: gọi api tạo order mới
   * nếu voucher không tồn tại: hiển thị modal cảnh báo
   */
  function callApiToCheckVoucherCode(paramVoucher, paramObj) {
    "use strict";
    $.ajax({
      type: "GET",
      url:
        "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" +
        paramVoucher,
      dataType: "json",
      success: function (paramResObj) {
        handleVoucherCode(paramResObj);
        //gửi đơn hàng lên api
        callApiCreateNewOrder(paramObj);
      },
      error: function (paramErr) {
        var vPrice = parseInt($("#modal-inp-thanhtien").val());
        //thông báo không tìm thấy mã voucher
        var vModalWarning = $("#modal-warning");
        var vModalWarningBody = $("#modal-body");
        vModalWarning.modal("show");
        vModalWarningBody.html("Mã voucher không tồn tại");
        //ẩn vùng giảm giá
        $("#div-show-giamgia").hide();
        //ghi ra số tiền phải thanh toán
        $("#modal-inp-thanhtoan").val(numberWithCommas(vPrice));
      },
    });
  }
  //Hàm xử lý tính tiền sau khi trừ giảm giá
  /**
   * INP:
   * @param {*} paramDiscount : phần trăm giảm giá
   * @param {*} paramPrice : đơn giá pizza
   * OUT:
   * @returns số tiền phải thanh toán sau khi giảm giá
   */
  function PriceAfterDiscount(paramDiscount, paramPrice) {
    "use strict";
    return paramPrice - paramPrice * (paramDiscount / 100);
  }

  //Hàm xử lý số tiền được giảm giá
  /**
   * INP:
   * @param {*} paramPizzaPrice : giá của pizza
   * @param {*} paramFinalPrice : giá sau khi được giảm
   * OUT:
   * @returns số tiền được giảm
   */
  function discountAmount(paramPizzaPrice, paramFinalPrice) {
    "use strict";
    return paramPizzaPrice - paramFinalPrice;
  }
  //Hàm xử lý sự kiện sau khi kiểm tra voucher thành công
  /**
   * INP:
   * @param {*} paramResObj : voucher đúng được api trả về
   * OUT:
   * hiển thị vùng giảm giá và giá tiền phải thanh toán
   */

  function handleVoucherCode(paramResObj) {
    "use strict";
    //Hiển thị các vùng giảm giá
    $("#div-show-giamgia").show();
    //Tính tiền phải thanh toán và tổng số tiền được giảm
    var vPizzaPrice = parseInt($("#modal-inp-thanhtien").val()); //đơn giá của pizza
    //số tiền phải thanh toán sau khi giảm giá
    var vTotalPrice = PriceAfterDiscount(
      paramResObj.phanTramGiamGia,
      vPizzaPrice
    );
    var vConvertTotalPrice = numberWithCommas(vTotalPrice); //quy đổi ra có dấu phẩy hàng ngàn
    //số tiền được giảm
    var vDiscountAmount = discountAmount(vPizzaPrice, vTotalPrice);
    var vConvertDiscountAmount = numberWithCommas(vDiscountAmount); //quy đổi ra dấu phẩy hàng ngàn
    //Hiển thị lên các trường input thanh toán và giảm giá trên modal chi tiết đơn hàng
    $("#modal-inp-thanhtoan").val(vConvertTotalPrice);
    $("#modal-inp-giamgia").val(vConvertDiscountAmount);
  }

  //Hàm thêm các trường dữ liệu html vào modal chi tiết đơn hàng khi ấn nút chi tiết
  function addMoreHtmlElement() {
    "use strict";
    //Thêm trường dữ liệu order id (chỉ khi ấn nút edit)
    $("#div-content-orderid").show(); // hiển thị vùng chứa order id
    $("#div-content-orderid").html(""); // clear tất cả dữ liệu có trong vùng
    var vDivContentOrderId = $("#div-content-orderid"); // truy xuất đến vùng chứa order id
    //tạo biến chứa html order id gồm 1 label và 1 thẻ input
    var vOrderIdHtml = `
        <label class="col-sm-3 col-form-label">Order Id</label>
        <div class="col-sm-9">
            <input type="text" class="form-control" id="input-orderid" placeholder="Order Id" readonly>
        </div> 
        `;
    //thêm vào vùng order id
    vDivContentOrderId.append(vOrderIdHtml);

    //Thêm html trạng thái đơn hàng
    $("#div-content-status").show(); // hiển thị vùng chứa trạng thái
    $("#div-content-status").html(""); // clear tất cả dữ liệu có trong vùng
    var vDivContentStatus = $("#div-content-status"); // truy xuất đến vùng chứa trạng thái đơn hàng
    //tạo biến chứa htmltrạng thái gồm 1 label và 1 select
    var vStatusHtml = `
        <label class="col-sm-3 col-form-label">Trạng thái</label>
        <div class="col-sm-9">
            <select id="select-status" class="form-control " style="width: 100%;">
                <option selected="selected" value="" disabled>Chọn trạng thái</option>
                <option value="open">Open</option>
                <option value="confirmed">Confirmed</option>
                <option value="cancel">Cancel</option>
                <option id="option-status" style="display:none;"></option>
                                           
            </select>  
        </div> 
        `;
    //thêm vào vùng trạng thái
    vDivContentStatus.append(vStatusHtml);

    //thêm option mới cho chọn kích cỡ
    var vSelectSize = $("#modal-select-kichco");
    var vSelectSizeHtml = `<option id="option-size"></option>`;
    vSelectSize.append(vSelectSizeHtml);

    //thêm option mới cho loại pizza
    var vSelectPizza = $("#modal-select-loaipizza");
    var vSelectPizzaHtml = `<option id="option-pizza"></option>`;
    vSelectPizza.append(vSelectPizzaHtml);

    //thêm option mới cho đồ uống
    var vSelectDrink = $("#modal-select-loainuocuong");
    var vSelectDrinkHtml = `<option id="option-drink"></option>`;
    vSelectDrink.append(vSelectDrinkHtml);
  }

  //Hàm thêm read only vào các trường dữ liệu khi ấn nút chi tiết
  function addReadOnlyToForm() {
    "use strict";
    //trường kích cỡ
    $("#modal-select-kichco").attr("disabled", true);
    //trường loại pizza
    $("#modal-select-loaipizza").attr("disabled", true);
    //trường mã voucher
    $("#modal-inp-mavoucher").prop("readonly", true);
    //trường loại nước uống
    $("#modal-select-loainuocuong").attr("disabled", true);
    //trường họ và tên
    $("#modal-inp-hovaten").prop("readonly", true);
    //trường số điện thoại
    $("#modal-inp-sodienthoai").prop("readonly", true);
    //trường email
    $("#modal-inp-email").prop("readonly", true);
    //trường địa chỉ
    $("#modal-inp-diachi").prop("readonly", true);
    //trường lời nhắn
    $("#modal-inp-loinhan").prop("readonly", true);
  }

  //Hàm show dữ liệu đơn hàng lên lên modal chi tiết đơn hàng khi ấn nút chi tiết
  /**
   * INP:
   * @param {*} paramOrderDetail: thông tin ORDER gắn liền với nút chi tiết
   * OUT:
   * thể hiện các thông tin đó lên modal chi tiêt đơn hàng
   */

  function showOrderDetailToModal(paramOrderDetail) {
    "use strict";
    //thông tin của khách hàng
    $("#modal-inp-hovaten").val(paramOrderDetail.hoTen);
    $("#modal-inp-sodienthoai").val(paramOrderDetail.soDienThoai);
    $("#modal-inp-email").val(paramOrderDetail.email);
    $("#modal-inp-diachi").val(paramOrderDetail.diaChi);

    //Hiển thị thông tin order id
    $("#input-orderid").val(paramOrderDetail.orderId);

    //Hiển thị thông tin kích cỡ combo lên modal
    var vSelectCombo = $("#modal-select-kichco").val(); // lấy giá trị kích cỡ
    if (paramOrderDetail.kichCo == vSelectCombo) {
      //nếu thông tin combo order trùng với kích cỡ combo trên modal chi tiết đơn hàng
      //thông tin trùng với value option có sẵn trong select
      //hiển thị thông tin combo lên select
      $("#modal-select-kichco").val(paramOrderDetail.kichCo);
      //thay đổi giá trị của select và css của select
      $("#modal-select-kichco")
        .select2({ theme: "bootstrap4" })
        .trigger("change");
    } else {
      //thông tin khác với value có sẵn trong option
      //truy xuất đến option mới được thêm vào khi ấn nút edit
      var vOptionSizeCombo = $("#option-size");
      //thêm value và text vào option size mới
      vOptionSizeCombo.val(paramOrderDetail.kichCo);
      vOptionSizeCombo.text(paramOrderDetail.kichCo);
      //truy xuất ngược giá trị của option size
      var vOptionComboValue = $("#option-size").val();
      //hiển thị thông tin lên modal chi tiết
      $("#modal-select-kichco").val(vOptionComboValue);
      //thay đổi giá trị của select và css của select
      $("#modal-select-kichco")
        .select2({ theme: "bootstrap4" })
        .trigger("change");
    }
    //thông tin chi tiết của các combo được chọn
    $("#modal-inp-duongkinh").val(paramOrderDetail.duongKinh);
    $("#modal-inp-suonnuong").val(paramOrderDetail.suon);
    $("#modal-inp-salad").val(paramOrderDetail.salad);
    $("#modal-inp-soluongnuoc").val(paramOrderDetail.soLuongNuoc);
    $("#modal-inp-thanhtien").val(numberWithCommas(paramOrderDetail.thanhTien));

    //Hiển thị thông tin loại pizza lên modal
    var vSelectPizza = $("#modal-select-loaipizza").val(); // lấy giá trị loại pizza
    if (paramOrderDetail.loaiPizza == vSelectPizza) {
      //nếu thông tin loại pizza trùng với loại pizza trên modal chi tiết đơn hàng
      //thông tin trùng với value option có sẵn trong select
      $("#modal-select-loaipizza").val(paramOrderDetail.loaiPizza);
      //thay đổi giá trị của select và css của select
      $("#modal-select-loaipizza")
        .select2({ theme: "bootstrap4" })
        .trigger("change");
    } else {
      //trong trương hợp value khác với 3 giá trị set cho select lúc ban đầu
      //truy xuất đến option mới được thêm vào khi ấn nút edit
      var vOptionPizza = $("#option-pizza");
      //thêm value và text vào option loại pizza mới
      vOptionPizza.val(paramOrderDetail.loaiPizza);
      vOptionPizza.text(paramOrderDetail.loaiPizza);
      //truy xuất ngược giá trị của option pizza
      var vOptionPizzaValue = $("#option-pizza").val();
      //hiển thị thông tin lên modal chi tiết
      $("#modal-select-loaipizza").val(vOptionPizzaValue);
      //thay đổi giá trị của select và css của select
      $("#modal-select-loaipizza")
        .select2({ theme: "bootstrap4" })
        .trigger("change");
    }

    //Hiển thị thông tin loại nước uống lên modal
    var vSelectDrink = $("#modal-select-loainuocuong").val(); // lấy giá trị loại đồ uống
    if (paramOrderDetail.idLoaiNuocUong == vSelectDrink) {
      //thông tin trường với value option ban đầu
      $("#modal-select-loainuocuong").val(paramOrderDetail.idLoaiNuocUong);
      //thay đổi giá trị của select và css của select
      $("#modal-select-loainuocuong")
        .select2({ theme: "bootstrap4" })
        .trigger("change");
    } else {
      //thông tin khác với value option ban đầu
      //truy xuất đến option mới được thêm vào khi ấn nút edit
      var vOptionDrink = $("#option-drink");
      //thêm value và text vào option loại nước uống
      vOptionDrink.val(paramOrderDetail.idLoaiNuocUong);
      vOptionDrink.text(paramOrderDetail.idLoaiNuocUong);
      //truy xuất ngược giá trị của option loại nước uống
      var vOptionDrinkValue = $("#option-drink").val();
      //hiển thị thông tin lên modal chi tiết
      $("#modal-select-loainuocuong").val(vOptionDrinkValue);
      //thay đổi giá trị của select và css của select
      $("#modal-select-loainuocuong")
        .select2({ theme: "bootstrap4" })
        .trigger("change");
    }

    //thông tin lời nhắn
    $("#modal-inp-loinhan").val(paramOrderDetail.loiNhan);
    //Thông tin mã voucher
    $("#modal-inp-mavoucher").val(paramOrderDetail.idVourcher);

    //Hiển thị thông tin trạng thái lên modal

    var vSelectStatus = $("#select-status").val();
    if (paramOrderDetail.trangThai == vSelectStatus) {
      //thông tin trường với value option ban đầu
      $("#select-status").val(paramOrderDetail.trangThai);
    } else {
      //thông tin khác với value option ban đầu
      var vOptionStatus = $("#option-status");
      vOptionStatus.val(paramOrderDetail.trangThai);
      vOptionStatus.text(paramOrderDetail.trangThai);
      var vOptionStatusValue = $("#option-status").val();
      $("#select-status").val(vOptionStatusValue);
    }

    //thông tin số tiền phải thanh toán
    $("#modal-inp-thanhtoan").val(
      numberWithCommas(paramOrderDetail.thanhTien - paramOrderDetail.giamGia)
    );
  }

  //Hàm gọi api update dữ liệu trạng thái đơn hàng
  /**
   * INP:
   * @param {*} paramObj :đối tượng ORDER muốn thay đổi trạng thái
   * OUT:trạng thái thay đổi được up lên api và load lại dữ liệu trên bảng
   */
  
  function callApiToUpdateOrderStatus(paramObj) {
    "use strict";
    $.ajax({
      type: "PUT",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramObj),
      success: function (paramResObj) {
        //gọi phương thức replaceObjAfterUpdated của gOrderDB để cập nhật lại đơn hàng
        gOrderDB.replaceObjAfterUpdated(paramResObj);
        //load dữ liệu đã update lên bảng
        loadUpdatedOrderToTable(gOrderDB.orders);
      },
      error: function (paramErr) {
          alert(paramErr)
      }
    });
  }
  //Hàm load dữ liệu đơn hàng đã update vào bảng
  /**
   * INP:
   * @param {*} paramOrderArr : danh sách đơn hàng
   * OUT: update lại danh sách đơn hàng
   */
  function loadUpdatedOrderToTable(paramOrderArr) {
    "use strict";
    gOrderTable.clear();
    gOrderTable.rows.add(paramOrderArr);
    gOrderTable.draw(false); //truyền dữ liệu false để khi update trạng thái đơn hàng sẽ update tại vị trí trang hiện tại mà không cần F5 lại trang
  }
  

  //Hàm gọi api để xoá dữ liệu đơn hàng
  function callApiToDeleteOrder() {
    "use strict";
    $.ajax({
      type: "DELETE",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders/" + gId,
      dataType: "json",
      success: function () {
        //thông báo cập nhật thành công
        alert("Đã xoá thành công đơn hàng");
        //Tắt modal
        $("#delete-modal").modal("hide");
        //Reset form
        resetForm();
        //Load lại trang
        location.reload();
      },
      error: function (paramErr) {
        alert(paramErr)
      }
    });
  }

  //Hàm reset các trường dữ liệu trong modal chi tiết đơn hàng

  function resetForm() {
    //reset trạng thái normal
    gFormMode = gFORM_MODE_NORMAL;
    $("#div-form-mod").html(gFormMode);
    ("use strict");
    //reset thông tin khách hàng
    $("#modal-inp-hovaten").val("");
    $("#modal-inp-sodienthoai").val("");
    $("#modal-inp-email").val("");
    $("#modal-inp-diachi").val("");

    //reset thông tin đơn hàng
    //ẩn vùng chứa orderid
    $("#div-content-orderid").hide();
    //reset dữ liệu cho select combo và xoá đi option thêm vào
    $("#modal-select-kichco").val("").trigger("change");
    $("#option-size").remove();
    //reset dữ liệu đi cùng với combo
    $("#modal-inp-duongkinh").val("");
    $("#modal-inp-suonnuong").val("");
    $("#modal-inp-salad").val("");
    $("#modal-inp-soluongnuoc").val("");
    $("#modal-inp-thanhtien").val("");
    //reset dữ liệu cho select loại pizza và xoá đi option thêm vào
    $("#modal-select-loaipizza").val("").trigger("change");
    $("#option-pizza").remove();
    //reset dữ liệu cho select loại nước uống và xoá đi option thêm vào
    $("#modal-select-loainuocuong").val("").trigger("change");
    $("#option-drink").remove();
    //reset mã voucher
    $("#modal-inp-mavoucher").val("");
    //reset lại lời nhắn
    $("#modal-inp-loinhan").val("");
    //ẩn đi vùng hiển thị trạng thái
    $("#div-content-status").hide();
    //reset lại phải thanh toán
    $("#modal-inp-thanhtoan").val("");

    //Hiện lại các nút xoá và vùng check voucher

    $("#div-show-giamgia").hide();

    //bỏ trạng thái readonly
    $("#modal-select-kichco").attr("disabled", false);
    $("#modal-select-loaipizza").attr("disabled", false);
    $("#modal-inp-mavoucher").prop("readonly", false);
    $("#modal-select-loainuocuong").attr("disabled", false);
    $("#modal-inp-hovaten").prop("readonly", false);
    $("#modal-inp-sodienthoai").prop("readonly", false);
    $("#modal-inp-email").prop("readonly", false);
    $("#modal-inp-diachi").prop("readonly", false);
    $("#modal-inp-loinhan").prop("readonly", false);
  }
});
