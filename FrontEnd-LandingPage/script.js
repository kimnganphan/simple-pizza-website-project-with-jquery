$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  //tạo biến chứa các thông tin combo được chọn (combo S, M, L)
  var gComboSelected = {
    combo: "",
    duongKinh: 0,
    suonNuong: 0,
    salad: 0,
    nuocngot: 0,
    giaTien: 0,
  };

  //tạo biến chứa loại pizza được chọn (hawai, bacon, seafood)
  var gPizzaTypeSelected = {
    pizzaType: "",
    pizzaName: "",
  };

  //tạo thông tin khách hàng mua pizza
  var gCustomerInfo = {
    idLoaiNuocUong: "",
    tenNuocUong: "",
    hoTen: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    idVourcher: "",
    loiNhan: "",
  };

  /*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */

  /*** vùng gán sự kiện cho các nút chọn combo */
  //gán sự kiện cho nút chọn combo size S
  $("#btn-small-combo").on("click", onBtnChooseSmallComboClick);
  //gán sự kiện cho nút chọn combo size M
  $("#btn-medium-combo").on("click", onBtnChooseMediumComboClick);
  //gán sự kiện cho nút chọn combo size L
  $("#btn-large-combo").on("click", onBtnChooseLargeComboClick);

  /*** vùng gán sự kiện cho các nút chọn loại pizza */
  //gán sự kiện cho nút chọn pizza hải sản(seafood)
  $("#btn-seafood").on("click", onBtnChooseSeafoodPizzaClick);
  //gán sự kiện cho nút chọn pizza hawaii
  $("#btn-hawai").on("click", onBtnChooseHawaiPizzaClick);
  //gán sự kiện cho nút chọn pizza Bacon
  $("#btn-bacon").on("click", onBtnChooseBaconPizzaClick);

  /*** vùng gán sự kiện cho nút gửi thông tin */
  $("#btn-send-data").on("click", onBtnSendDataClick);

  /** vùng gán sự kiện cho nút tạo đơn hàng */
  $("#btn-create-order").on("click", onBtnCreateOrderClick);

  //gán sự kiện cho nút to the top quay về đầu trang
  $("#btn-to-top").on("click", function () {
    $("html, body").animate({ scrollTop: 0 }, 1000);
  });
  //thực thi hàm onPageLoading
  onPageLoading();
  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  //hàm xử lý sự kiện load trang
  function onPageLoading() {
    "use strict";
    //load đồ uống vào select loại đồ uống
    callAjaxApiToLoadDrinkSelect();
  }
  //hàm xử lý sự kiện ấn nút chọn combo size S
  function onBtnChooseSmallComboClick() {
    "use strict";
    //Truy xuất id các nút
    var vBtnSmall = $("#btn-small-combo");
    var vBtnMedium = $("#btn-medium-combo");
    var vBtnLarge = $("#btn-large-combo");

    //đổi màu nút khi ấn
    changeButtonColorAfterClick(vBtnSmall, vBtnMedium, vBtnLarge);

    //thu thập dữ liệu cho gComboSelected khi ấn chọn combo S
    gComboSelected.combo = "S";
    gComboSelected.duongKinh = 20;
    gComboSelected.suonNuong = 2;
    gComboSelected.salad = 200;
    gComboSelected.nuocngot = 2;
    gComboSelected.giaTien = 150000;
  }
  //hàm xử lý sự kiện ấn nút chọn combo size M
  function onBtnChooseMediumComboClick() {
    "use strict";
    //Truy xuất id các nút
    var vBtnSmall = $("#btn-small-combo");
    var vBtnMedium = $("#btn-medium-combo");
    var vBtnLarge = $("#btn-large-combo");
    //đổi màu nút khi ấn
    changeButtonColorAfterClick(vBtnMedium, vBtnSmall, vBtnLarge);
    //thu thập dữ liệu cho gComboSelected khi ấn chọn combo M
    gComboSelected.combo = "M";
    gComboSelected.duongKinh = 25;
    gComboSelected.suonNuong = 4;
    gComboSelected.salad = 300;
    gComboSelected.nuocngot = 3;
    gComboSelected.giaTien = 200000;
  }
  //hàm xử lý sự kiện ấn nút chọn combo size L
  function onBtnChooseLargeComboClick() {
    "use strict";
    //Truy xuất id các nút
    var vBtnSmall = $("#btn-small-combo");
    var vBtnMedium = $("#btn-medium-combo");
    var vBtnLarge = $("#btn-large-combo");
    //đổi màu nút khi ấn
    changeButtonColorAfterClick(vBtnLarge, vBtnSmall, vBtnMedium);
    //thu thập dữ liệu cho gComboSelected khi ấn chọn combo L
    gComboSelected.combo = "L";
    gComboSelected.duongKinh = 30;
    gComboSelected.suonNuong = 8;
    gComboSelected.salad = 500;
    gComboSelected.nuocngot = 4;
    gComboSelected.giaTien = 250000;
  }

  //hàm xử lý sự kiện ấn nút chọn pizza hải sản(seafood)
  function onBtnChooseSeafoodPizzaClick() {
    "use strict";
    //Truy xuất id các nút
    var vBtnSeafood = $("#btn-seafood");
    var vBtnHawai = $("#btn-hawai");
    var vBtnBacon = $("#btn-bacon");
    //đổi màu nút khi ấn
    changeButtonColorAfterClick(vBtnSeafood, vBtnHawai, vBtnBacon);
    //thu thập dữ liệu cho gPizzaTypeSelected khi ấn nút chọn pizza hải sản
    gPizzaTypeSelected.pizzaType = "seafood";
    gPizzaTypeSelected.pizzaName = "OCEAN MANIA";
  }
  //hàm xử lý sự kiện ấn nút chọn pizza hawaii
  function onBtnChooseHawaiPizzaClick() {
    "use strict";
    //Truy xuất id các nút
    var vBtnSeafood = $("#btn-seafood");
    var vBtnHawai = $("#btn-hawai");
    var vBtnBacon = $("#btn-bacon");
    //đổi màu nút khi ấn
    changeButtonColorAfterClick(vBtnHawai, vBtnSeafood, vBtnBacon);
    //thu thập dữ liệu cho gPizzaTypeSelected khi ấn nút chọn pizza hawai
    gPizzaTypeSelected.pizzaType = "hawaii";
    gPizzaTypeSelected.pizzaName = "HAWAIIAN";
  }
  //hàm xử lý sự kiện ấn nút chọn pizza bacon
  function onBtnChooseBaconPizzaClick() {
    "use strict";
    //Truy xuất id các nút
    var vBtnSeafood = $("#btn-seafood");
    var vBtnHawai = $("#btn-hawai");
    var vBtnBacon = $("#btn-bacon");
    //đổi màu nút khi ấn
    changeButtonColorAfterClick(vBtnBacon, vBtnHawai, vBtnSeafood);
    //thu thập dữ liệu cho gPizzaTypeSelected khi ấn nút chọn pizza bacon
    gPizzaTypeSelected.pizzaType = "bacon";
    gPizzaTypeSelected.pizzaName = "CHEESY CHICKEN BACON";
  }

  //Hàm xử lý sự kiện ấn nút gửi thông tin trong phần gửi đơn hàng
  function onBtnSendDataClick() {
    "use strict";
    //thu thập thông tin của khách hàng mua pizza (gCustomerInfo)
    getCustomerInfo(gCustomerInfo);

    //Kiểm tra thông tin của khách hàng đã gửi
    var vIsValidated = validateCustomerInfo(gCustomerInfo);

    //nếu các thông tin của khách hàng hợp lệ => xử lý thông tin khách hàng
    if (vIsValidated) {
      //trường hợp không nhập mã voucher
      if (gCustomerInfo.idVourcher == "") {
        //thông tin mã voucher được hiển thị vào thông tin chi tiết đơn hàng
        var vVoucherContent = "Bạn không có mã voucher";
        //hiển thị modal xác nhận thông tin đơn hàng
        $("#order-detail-modal").modal("show");
        //hiển thị các thông tin đơn hàng đã được validate vào modal
        showOrderDetailToModal(
          gCustomerInfo,
          vVoucherContent,
          gComboSelected.giaTien
        );
      } else {
        // nếu khách hàng có nhập voucher, kiểm tra voucher xem có hợp lệ hay không
        callApiCheckVoucherId(gCustomerInfo.idVourcher);
      }
    }
  }

  //hàm xử lý sự kiện ấn nút tạo đơn hàng
  function onBtnCreateOrderClick() {
    "use strict";

    //tạo đối tượng chứa thông tin đơn hàng sẽ tạo
    var vOrderRequest = {
      kichCo: "",
      duongKinh: "",
      suon: "",
      salad: "",
      loaiPizza: "",
      idVourcher: "",
      idLoaiNuocUong: "",
      soLuongNuoc: "",
      hoTen: "",
      thanhTien: "",
      email: "",
      soDienThoai: "",
      diaChi: "",
      loiNhan: "",
    };

    //thu thập thông tin đơn hàng
    vOrderRequest.kichCo = gComboSelected.combo;
    vOrderRequest.duongKinh = gComboSelected.duongKinh;
    vOrderRequest.suon = gComboSelected.suonNuong;
    vOrderRequest.salad = gComboSelected.salad;
    vOrderRequest.loaiPizza = gPizzaTypeSelected.pizzaType;
    vOrderRequest.idVourcher = gCustomerInfo.idVourcher;
    vOrderRequest.idLoaiNuocUong = gCustomerInfo.idLoaiNuocUong;
    vOrderRequest.soLuongNuoc = gComboSelected.nuocngot;
    vOrderRequest.hoTen = gCustomerInfo.hoTen;
    vOrderRequest.thanhTien = gComboSelected.giaTien;
    vOrderRequest.email = gCustomerInfo.email;
    vOrderRequest.soDienThoai = gCustomerInfo.soDienThoai;
    vOrderRequest.diaChi = gCustomerInfo.diaChi;
    vOrderRequest.loiNhan = gCustomerInfo.loiNhan;

    //tạo đơn hàng
    callApiToCreateNewOrder(vOrderRequest);
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  //Hàm xử lý sự kiện đổi màu nút khi ấn
  /***INP:
   * paramButtonClicked: id của nút được ấn
   * paramOtherButton1, paramOtherButton2: các nút còn lại*/
  //OUT: nút được ấn đổi màu, 2 nút còn lại giữ nguyên màu cũ
  function changeButtonColorAfterClick(
    paramButtonClicked,
    paramOtherButton1,
    paramOtherButton2
  ) {
    "use strict";
    $(paramButtonClicked).removeClass("btn-warning").addClass("btn-success");
    $(paramOtherButton1).removeClass("btn-warning").addClass("btn-warning");
    $(paramOtherButton2).removeClass("btn-warning").addClass("btn-warning");
  }

  //hàm xử lý gọi api lấy dữ liệu đồ uống load vào select loại đồ uống
  function callAjaxApiToLoadDrinkSelect() {
    "use strict";
    $.ajax({
      type: "GET",
      url: "http://42.115.221.44:8080/devcamp-pizza365/drinks",
      dataType: "json",
      success: function (responseObj) {
        //thêm drink vào select
        loadDrinkListToSelect(responseObj);
      },
      error: function (err) {
        console.log(err);
      },
    });
  }

  //Hàm load dữ liệu đồ uống vào select
  //   INP: paramDrinkArr : danh sách đồ uống gọi từ api
  // OUT: load dữ liệu đồ uống gọi từ Api vào select đồ uống
  function loadDrinkListToSelect(paramDrinkArr) {
    "use strict";
    //truy xuất id của select drink
    var vDrinkSelect = $("#select-drink");
    for (var bI = 0; bI < paramDrinkArr.length; bI++) {
      vDrinkSelect.append(
        $("<option>", {
          value: paramDrinkArr[bI].maNuocUong,
          text: paramDrinkArr[bI].tenNuocUong,
        })
      );
    }
  }

  //hàm thu thập dữ liệu khách hàng
  //Inp: đối tượng cần thu thập dữ liệu (paramCustomerInfo)
  //Out: dữ liệu mà khách hàng đã điền vào
  function getCustomerInfo(paramCustomerInfo) {
    "use strict";

    //thu thập thông tin nước uống khách hàng đã chọn
    paramCustomerInfo.idLoaiNuocUong = $("#select-drink").val();
    paramCustomerInfo.tenNuocUong = $("#select-drink :selected").text();

    //thu thập thông tin họ tên khách hàng
    paramCustomerInfo.hoTen = $("#inp-fullname").val().trim();
    //thông tin email của khách hàng
    paramCustomerInfo.email = $("#inp-email").val().trim();
    //thông tin số điện thoại của khách hàng
    paramCustomerInfo.soDienThoai = $("#inp-phone-number").val().trim();
    //thông tin địa chỉ của khách hàng
    paramCustomerInfo.diaChi = $("#inp-address").val().trim();
    //thông tin mã voucher của khách hàng
    paramCustomerInfo.idVourcher = $("#inp-voucher").val().trim();
    //thông tin lời nhắn của khách hàng
    paramCustomerInfo.loiNhan = $("#inp-message").val().trim();
  }

  //Hàm kiểm tra thông tin khách hàng
  /** INP:
   * paramCustomerInfo : thông tin khách hàng lấy từ phần gửi đơn hàng
   * gComboSelected : thông tin combo
   * gPizzaTypeSelected: loại pizza
   */
  //OUT: nếu thông tin hợp lệ sẽ trả về true, nếu thông tin ko hợp lệ sẽ hiển thị modal cảnh báo
  function validateCustomerInfo(paramCustomerInfo) {
    "use strict";
    //truy xuất đến alert modal và alert modal content
    var vAlertModal = $("#alert-modal");
    var vAlertModalContent = $("#alert-content");

    //validate phần chọn combo
    if (gComboSelected.combo == "") {
      vAlertModal.modal("show");
      vAlertModalContent.html("Xin vui lòng chọn combo");
      return false;
    }

    //validate phần chọn loại pizza
    if (gPizzaTypeSelected.pizzaType == "") {
      vAlertModal.modal("show");
      vAlertModalContent.html("Xin vui lòng chọn loại pizza");
      return false;
    }

    //validate chọn đồ uống
    if (paramCustomerInfo.idLoaiNuocUong == "") {
      vAlertModal.modal("show");
      vAlertModalContent.html("Xin vui lòng chọn loại đồ uống");
      return false;
    }

    //validate họ tên khách hàng
    if (paramCustomerInfo.hoTen == "") {
      vAlertModal.modal("show");
      vAlertModalContent.html("Xin vui lòng nhập họ và tên");
      return false;
    }

    //validate email của khách hàng
    var vValidEmail = validateEmail(paramCustomerInfo.email);
    if (!vValidEmail) {
      vAlertModal.modal("show");
      vAlertModalContent.html(
        "Email của quý khách không đúng định dạng (xyz@example.com)"
      );
      return false;
    }
    //validate số điện thoại của khách hàng
    var vValidPhoneNumber = validatePhoneNumber(paramCustomerInfo.soDienThoai);
    if (paramCustomerInfo.soDienThoai == "") {
      vAlertModal.modal("show");
      vAlertModalContent.html("Xin vui lòng nhập số điện thoại");
      return false;
    }
    if (!vValidPhoneNumber) {
      vAlertModal.modal("show");
      vAlertModalContent.html("Số điện thoại của quý khách không hợp lệ");
      return false;
    }
    //validate địa chỉ của khách hàng
    if (paramCustomerInfo.diaChi == "") {
      vAlertModal.modal("show");
      vAlertModalContent.html("Xin vui lòng nhập địa chỉ");
      return false;
    }
    return true;
  }

  //hàm validate tính hợp lệ email của khách hàng
  function validateEmail(paramEmail) {
    "use strict";
    var vFormat =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail == "") {
      return true;
    } else {
      if (!vFormat.test(paramEmail)) {
        return false;
      }
    }

    return true;
  }

  //hàm validate số điện thoại của khách hàng
  function validatePhoneNumber(paramPhoneNumber) {
    "use strict";
    var vFormat = /^0([1-2]\d{9}|[3-9]\d{8})$/;
    if (!vFormat.test(paramPhoneNumber)) {
      return false;
    }
    return true;
  }

  //Hàm hiển thị dấu phẩy giữa hàng ngàn
  //INP: paramNumber: số tiền
  //OUT: trả về số tiền có dấu phẩy giữa đơn vị hàng ngàn
  function numberWithCommas(paramNumber) {
    "use strict";
    return paramNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  //hàm xử lý hiển thị thông tin đơn hàng hợp lệ lên modal thông tin đơn hàng
  /** INP:
   * paramCustomerInfo: thông tin khách hàng (gCustomerInfo)
   * paramVoucher : mã voucher
   * paramFinalPrice: số tiền phải thanh toán
   * thông tin combo, loại pizza và đồ uống đã chọn
   */
  //OUT: Các thông tin chi tiết của đơn hàng được hiển thị lên modal
  function showOrderDetailToModal(
    paramCustomerInfo,
    paramVoucher,
    paramFinalPrice
  ) {
    "use strict";
    //hiển thị thông tin họ và tên của khách hàng
    $("#inp-modal-fullname").val(paramCustomerInfo.hoTen);
    //hiển thị thông tin email của khách hàng
    $("#inp-modal-email").val(paramCustomerInfo.email);
    //hiển thị thông tin số điện thoại của khách hàng
    $("#inp-modal-phone-number").val(paramCustomerInfo.soDienThoai);
    //hiển thị thông tin địa chỉ của khách hàng
    $("#inp-modal-address").val(paramCustomerInfo.diaChi);
    //hiển thị lời nhắn của khách hàng
    $("#inp-modal-message").val(paramCustomerInfo.loiNhan);
    //hiển thị thông tin mã voucher
    $("#inp-modal-voucher").val(paramCustomerInfo.idVourcher);

    //giá tiền của pizza sau khi được đổi sang có dấu phẩy hàng ngàn
    var vPriceAfterConvertWithCommas = numberWithCommas(gComboSelected.giaTien);

    //hiển thị thông tin chi tiết của đơn hàng
    $("#inp-modal-detail").val(function () {
      return (
        `Xác nhận thông tin khách hàng:` +
        "\n" +
        `Họ và tên: ${paramCustomerInfo.hoTen}` +
        "\n" +
        `Số điện thoại: ${paramCustomerInfo.soDienThoai}` +
        "\n" +
        `Địa chỉ: ${paramCustomerInfo.diaChi}` +
        "\n" +
        `--------------------------------------------------------------------------------------------------------` +
        "\n" +
        `Combo: ${gComboSelected.combo}` +
        "\n" +
        `Đường kính: ${gComboSelected.duongKinh} cm` +
        "\n" +
        `Sườn nướng: ${gComboSelected.suonNuong} miếng` +
        "\n" +
        `Salad: ${gComboSelected.salad} gram` +
        "\n" +
        `Số lượng nước: ${gComboSelected.nuocngot}` +
        "\n" +
        `--------------------------------------------------------------------------------------------------------` +
        "\n" +
        `Loại pizza: ${gPizzaTypeSelected.pizzaName}` +
        "\n" +
        `Loại đồ uống: ${paramCustomerInfo.tenNuocUong}` +
        "\n" +
        `Gía: ${vPriceAfterConvertWithCommas} VNĐ` +
        "\n" +
        `Mã voucher: ${paramVoucher}` +
        "\n" +
        `Phải thanh toán: ${numberWithCommas(paramFinalPrice)} VNĐ`
      );
    });
  }
  //Hàm xử lý số tiền phải thanh toán nếu có mã voucher
  /** INP:
   * paramDiscount: phần trăm giảm giá
   * paramPrice: giá tiền ban đầu 
   */
  //OUT: số tiền đã được giảm giá
  function finalPriceAfterEnterVoucher(paramDiscount, paramPrice) {
    "use strict";
    return paramPrice - paramPrice * (paramDiscount / 100);
  }
  //hàm kiểm tra voucher
  /** INP:
   * paramVoucher: mã voucher được nhập từ phần gửi đơn hàng
   * 
   */
  //OUT: voucher có hợp lệ hay không, nếu hợp lệ sẽ hiển thị lên modal thông tin chi tiết đơn hàng. Nếu không hợp lệ sẽ hiển thị modal cảnh báo
  function callApiCheckVoucherId(paramVoucher) {
    "use strict";
    $.ajax({
      type: "GET",
      url:
        "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" +
        paramVoucher,
      dataType: "json",
      success: function (responseObj) {
        //hiển thị modal thông tin đơn hàng
        $("#order-detail-modal").modal("show");
        //số tiền phải thanh toán
        var vTotalPrice = finalPriceAfterEnterVoucher(
          responseObj.phanTramGiamGia,
          gComboSelected.giaTien
        );
        //hiển thị mã voucher vào trong modal thông tin đơn hàng
        showOrderDetailToModal(
          gCustomerInfo,
          responseObj.maVoucher,
          vTotalPrice
        );
      },
      error: function (err) {
        //hiển thị modal cảnh báo
        $("#alert-modal").modal("show");
        //nội dung cảnh báo
        $("#alert-content").html(
          "Mã voucher của quý khách không tồn tại. Xin quý khách vui lòng kiểm tra lại"
        );
      },
    });
  }

  //hàm xử lý sự kiện gọi api tạo đơn hàng
  /** INP:
   * paramOrderRequest: đối tượng chứa thông tin đơn hàng
   * 
   */
  //OUT: đơn hàng được tạo thành công (trả về thông tin đơn hàng đã tạo)
  function callApiToCreateNewOrder(paramOrderRequest) {
    "use strict";
    $.ajax({
      type: "POST",
      url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
      contentType: "application/json;charset=UTF-8",
      data: JSON.stringify(paramOrderRequest),
      success: function (responseObj) {
        //ẩn đi modal thông tin đơn hàng
        $("#order-detail-modal").modal("hide");
        //hiển thị modal thông tin mã đơn hàng thành công
        $("#order-modal").modal("show");
        //hiển thị mã đơn hàng lên input mã đơn hàng
        $("#inp-madonhang").val(responseObj.orderId);
        $("#order-modal").on("hidden.bs.modal", function () {
          //reset lại trang web sau khi ấn tắt modal mã đơn hàng
          window.location.reload();
        });
      },
      error: function (ajaxContext) {
        alert(ajaxContext.responseText);
      },
    });
  }
});
